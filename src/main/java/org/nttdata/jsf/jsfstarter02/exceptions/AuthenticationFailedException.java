package org.nttdata.jsf.jsfstarter02.exceptions;

public class AuthenticationFailedException extends RuntimeException {

	public AuthenticationFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthenticationFailedException() {
		super("This user does not exist.");
	}
}
