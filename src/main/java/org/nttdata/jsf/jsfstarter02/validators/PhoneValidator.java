package org.nttdata.jsf.jsfstarter02.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

@FacesValidator("phoneValidator")
public class PhoneValidator implements Validator<String> {

	@Override
	public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
		String phone = value.trim();
		if (phone.isEmpty())
			throw new ValidatorException(new FacesMessage("Enter your cell phone"));
		else {
			String pattern = "^9[0-9]{8}$";
			boolean ok = Pattern.matches(pattern, phone);
			if (!ok)
				throw new ValidatorException(new FacesMessage("Invalid format"));
		}
	}
}
