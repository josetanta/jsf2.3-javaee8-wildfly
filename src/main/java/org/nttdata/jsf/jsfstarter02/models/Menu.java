package org.nttdata.jsf.jsfstarter02.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name = "menus")
@AllArgsConstructor
@NoArgsConstructor
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String name;
	private String type;
	private String typeUser;

	@ManyToOne
	@JoinColumn
	private Menu subMenu;

	private String url;

	private Short status;
}
