package org.nttdata.jsf.jsfstarter02.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "notes")
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String header;
	private String body;
	private Date createdAt;
	private String comment;
	private String commentOfAdmin;
	private short valorisation;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Person owner;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Category category;
}
