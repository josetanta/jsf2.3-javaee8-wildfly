package org.nttdata.jsf.jsfstarter02.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private Boolean status;
}
