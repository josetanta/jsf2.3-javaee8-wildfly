package org.nttdata.jsf.jsfstarter02.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.enterprise.inject.Default;
import javax.persistence.*;

@Default
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "phones")
public class Phone {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String number;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	private Person owner;
}
