package org.nttdata.jsf.jsfstarter02.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.nttdata.jsf.jsfstarter02.services.authenticate.UserDetails;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable, UserDetails {
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id", nullable = false)
	private Person owner;

	private String username;
	private String password;
	private String type;
}
