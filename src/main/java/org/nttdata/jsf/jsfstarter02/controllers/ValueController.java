package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.nttdata.jsf.jsfstarter02.exceptions.ResourceNotFoundException;
import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.services.NoteService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Slf4j
@Named("valueMB")
@ViewScoped
public class ValueController implements Serializable {

	@Inject
	private MentionController mentionController;

	@EJB
	private NoteService noteService;

	@Getter
	@Setter
	private Note note;

	@PostConstruct
	public void init() {
		setNote(mentionController.getNote());
	}

	public void registerNote() {
		var facesCurrent = FacesContext.getCurrentInstance();
		log.info("update note with comment of admin note - {}", getNote());
		try {
			noteService.update(note, note.getId());
			facesCurrent.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Register note with successfully."));
		} catch (ResourceNotFoundException ex) {
			facesCurrent.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "There are anything error."));
		}
	}
}
