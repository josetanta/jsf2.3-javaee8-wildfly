package org.nttdata.jsf.jsfstarter02.controllers;

import org.nttdata.jsf.jsfstarter02.models.User;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Objects;

@Named
@SessionScoped
public class TemplateController implements Serializable {

	public void isAuth() {

		var facesCurrent = FacesContext.getCurrentInstance().getExternalContext();
		try {
			User user = (User) facesCurrent.getSessionMap().get("session-us");
			if (Objects.isNull(user))
				facesCurrent.redirect(facesCurrent.getRequestContextPath() + "/permissions.myapp");
		} catch (Exception e) {
			// errrors
		}
	}
}
