package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.services.NoteService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Slf4j
@Named("mentionMB")
@RequestScoped
public class MentionController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private NoteService noteService;

	@Getter
	private List<Note> notes;

	@Getter
	@Setter
	private Note note;

	@PostConstruct
	public void init() {
		this.notes = noteService.list();
	}

	public void assign(Note note) {
		log.info("info for setting node {}", note);
		setNote(note);
	}
}
