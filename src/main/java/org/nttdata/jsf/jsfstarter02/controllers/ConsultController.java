package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import org.nttdata.jsf.jsfstarter02.models.Person;
import org.nttdata.jsf.jsfstarter02.models.Phone;
import org.nttdata.jsf.jsfstarter02.repository.persistence.PhoneRepository;
import org.nttdata.jsf.jsfstarter02.services.PersonService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Named
@ViewScoped
public class ConsultController implements Serializable {

	@EJB
	private PersonService personService;

	@EJB
	private PhoneRepository phoneRepository;

	@Getter
	private List<Person> people;

	@Getter
	private Set<Phone> phones;

	@Getter
	@Setter
	private int personID;

	@PostConstruct
	private void init() {
		people = personService.listOfPerson();
	}

	public void searchPhoneAction() {
		phones = phoneRepository.findAllPhonesByOwnerId(personID);

	}
}
