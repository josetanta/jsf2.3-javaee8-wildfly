package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import org.nttdata.jsf.jsfstarter02.models.Category;
import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.models.Person;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class NoteController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Note note;

	@Getter
	@Setter
	private Category category;

	@Getter
	@Setter
	private Person person;

	@Getter
	@Setter
	private List<Category> categories;

	@Getter
	@Setter
	private List<Note> notes;
}
