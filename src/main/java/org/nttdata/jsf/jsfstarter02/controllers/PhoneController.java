package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import org.nttdata.jsf.jsfstarter02.models.Phone;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.repository.persistence.PhoneRepository;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Set;

@Named("phone")
@ViewScoped
public class PhoneController implements Serializable {

	@EJB
	private PhoneRepository phoneRepository;

	@Getter
	@Setter
	private Phone phoneBean;

	@Getter
	private Set<Phone> phones;

	private User user;

	@Getter
	@Setter
	private String commandLabelAction;

	@PostConstruct
	public void init() {
		setPhoneBean(new Phone());
		user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("session-us");
		phones = phoneRepository.findAllPhonesByOwnerId(user.getOwner().getId());
	}

	public void changeCommandLabel() {
		setPhoneBean(null);
		setCommandLabelAction("R");
	}

	public void registerPhoneAction() {
		phoneBean.setOwner(user.getOwner());
		phoneRepository.persist(phoneBean);
		phones = phoneRepository.findAllPhonesByOwnerId(user.getOwner().getId());
		setPhoneBean(new Phone());
	}

	public void saveChangesAction(Phone phone) {
		phoneRepository.update(phone);
	}

	public void readPhoneAction(Phone phone) {
		setPhoneBean(phone);
		setCommandLabelAction("M");
	}

}
