package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.nttdata.jsf.jsfstarter02.models.Person;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.services.PersonService;
import org.nttdata.jsf.jsfstarter02.services.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Model;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Slf4j
@Named("personMB")
@Model
public class PersonController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private UserService userService;

	@EJB
	private PersonService personService;

	@Getter
	@Setter
	private User user;

	@Getter
	@Setter
	private Person person;

	@Getter
	private List<Person> people;

	@PostConstruct
	public void init() {
		person = new Person();
		user = new User();
		people = personService.listOfPerson();
		log.info("init values for person, user and people [{}]", getPeople());
	}

	public String register() {
		log.info("information user [{}] person [{}]", getUser(), getPerson());
		personService.registerPerson(person);
		user.setOwner(person);
		userService.register(user);
		return "result-information?faces-redirect=true";
	}
}
