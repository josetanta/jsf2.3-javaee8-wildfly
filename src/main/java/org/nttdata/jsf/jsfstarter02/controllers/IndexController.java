package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.services.UserService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Slf4j
@Named
@ViewScoped
public class IndexController implements Serializable {

	@Inject
	private UserService userService;

	@Getter
	@Setter
	private User user;

	@PostConstruct
	public void init() {
		user = new User();
	}

	public String login() {
		var facesInstance = FacesContext.getCurrentInstance();

		User findUser = null;
		try {
			findUser = (User) userService.authenticate(getUser());
			facesInstance.getExternalContext().getSessionMap().put("session-us", findUser);
			log.info("infor user {}", findUser);

			return "/home?faces-redirect=true";
		} catch (RuntimeException ex) {
			facesInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", ex.getMessage()));
			log.error("error {}", ex.getMessage());
		}
		return null;
	}
}
