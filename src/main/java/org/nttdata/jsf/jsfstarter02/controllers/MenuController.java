package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import org.nttdata.jsf.jsfstarter02.models.Menu;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.services.MenuService;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Named
@SessionScoped
public class MenuController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private MenuService menuService;

	@Getter
	@Setter
	private List<Menu> menuList;

	@Getter
	@Setter
	private MenuModel menuModel;

	@PostConstruct
	public void init() {
		setMenuList(menuService.list());
		setMenuModel(new DefaultMenuModel());
		buildMenuList(menuList);
	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

	private void buildMenuList(List<Menu> menuList) {
		// type menu is: 'S', 'I' ( S: Submenu, I: Item )
		var ce = FacesContext.getCurrentInstance().getExternalContext();
		User user = (User) ce.getSessionMap().get("session-us");

		if (Objects.isNull(user))
			return;

		var appPath = ce.getApplicationContextPath();

		menuList.forEach(m01 -> {
			boolean isSubmenu = m01.getType().equals("S");
			boolean typeuser = m01.getTypeUser().equals(user.getType());

			if (isSubmenu && typeuser) {
				DefaultSubMenu newsubmenu = DefaultSubMenu.builder().label(m01.getName()).id(m01.getId().toString())
					.build();

				menuList.forEach(m02 -> {
					boolean isItem = m02.getType().equals("I");
					boolean hasSubMenu = !Objects.isNull(m02.getSubMenu());
					boolean typeuser02 = m02.getTypeUser().equals(user.getType());

					if (isItem && hasSubMenu && m02.getSubMenu().getId().equals(m01.getId()) && typeuser02) {
						DefaultMenuItem newItem = DefaultMenuItem.builder().value(m02.getName())
							.url(appPath + m02.getUrl()).build();
						newsubmenu.getElements().add(newItem);
					}
				});

				menuModel.getElements().add(newsubmenu);
			} else if (!isSubmenu && Objects.isNull(m01.getSubMenu()) && typeuser) {
				DefaultMenuItem newItem = DefaultMenuItem.builder().value(m01.getName()).url(appPath + m01.getUrl())
					.build();
				menuModel.getElements().add(newItem);
			}
		});
	}
}
