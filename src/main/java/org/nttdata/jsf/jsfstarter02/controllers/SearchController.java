package org.nttdata.jsf.jsfstarter02.controllers;

import lombok.Getter;
import lombok.Setter;
import org.nttdata.jsf.jsfstarter02.models.Category;
import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.repository.persistence.CategoryRepository;
import org.nttdata.jsf.jsfstarter02.services.NoteService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Named("search")
@ViewScoped
public class SearchController implements Serializable {

	@Getter
	private Set<Category> categories;

	@Getter
	private List<Note> notes;

	@Getter
	@Setter
	private int idCategory;

	@Getter
	@Setter
	private Date dateSearch;

	@EJB
	private CategoryRepository categoryRepository;

	@EJB
	private NoteService noteService;

	@PostConstruct
	public void init() {
		categories = categoryRepository.findAll().stream().collect(Collectors.toSet());
	}

	public void searchAction() {
		notes = noteService.searchNotesByCategoryAndDate(idCategory, dateSearch);
	}
}
