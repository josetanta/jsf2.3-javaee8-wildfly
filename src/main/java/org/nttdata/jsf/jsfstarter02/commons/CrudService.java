package org.nttdata.jsf.jsfstarter02.commons;

import java.util.List;

public interface CrudService<T, ID> {
	List<T> list();

	void create(T obj);

	void update(T obj, ID id);

	void delete(ID id);

	T findById(ID id);
}
