package org.nttdata.jsf.jsfstarter02.repository.adapater;

import org.nttdata.jsf.jsfstarter02.models.Category;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.AbstractRepository;
import org.nttdata.jsf.jsfstarter02.repository.persistence.CategoryRepository;

import javax.ejb.Stateless;

@Stateless
public class CategoryRepositoryAdapter extends AbstractRepository<Category, Integer> implements CategoryRepository {

	public CategoryRepositoryAdapter() {
		super(Category.class);
	}

}
