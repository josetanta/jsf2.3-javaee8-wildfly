package org.nttdata.jsf.jsfstarter02.repository.adapater;

import org.nttdata.jsf.jsfstarter02.models.Phone;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.AbstractRepository;
import org.nttdata.jsf.jsfstarter02.repository.persistence.PhoneRepository;

import javax.ejb.Stateless;
import java.util.Set;
import java.util.stream.Collectors;

@Stateless
public class PhoneRespositoryAdapter extends AbstractRepository<Phone, Integer> implements PhoneRepository {

	public PhoneRespositoryAdapter() {
		super(Phone.class);
	}

	@Override
	public void persist(Phone e) {
		// TODO Auto-generated method stub
		super.persist(e);
	}

	@Override
	public Set<Phone> findAllPhonesByOwnerId(Integer ownerId) {
		String jpql = "from Phone p where p.owner.id=?1";
		var phones = em.createQuery(jpql, Phone.class)
			.setParameter(1, ownerId)
			.getResultList()
			.stream()
			.collect(Collectors.toSet());

		return phones;
	}
}
