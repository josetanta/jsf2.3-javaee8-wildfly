package org.nttdata.jsf.jsfstarter02.repository.adapater;

import org.nttdata.jsf.jsfstarter02.models.Person;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.AbstractRepository;
import org.nttdata.jsf.jsfstarter02.repository.persistence.PersonRepository;

import javax.ejb.Stateless;

@Stateless
public class PersonRepositoryAdapater extends AbstractRepository<Person, Integer> implements PersonRepository {

	public PersonRepositoryAdapater() {
		super(Person.class);
	}

}
