package org.nttdata.jsf.jsfstarter02.repository.abstract_repository;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Optional;

public abstract class AbstractRepository<E, ID> extends AbstractRepositoryReflect<E> {

	@ApplicationScoped
	@PersistenceContext
	protected EntityManager em;

	public AbstractRepository(final Class<E> entityClass) {
		super(entityClass);
		loadNameEntity();
		loadNameID();
	}

	public Optional<E> findById(ID id) {
		String jpql = String.format("select e from %s e where %s=?1", NAME_ENTITY, NAME_ID);

		E result = em.createQuery(jpql, entity.get()).setParameter(1, id).getSingleResult();

		return Optional.of(result);
	}

	public Collection<E> findAll() {
		return em.createQuery("from " + NAME_ENTITY, entity.get()).getResultList();
	}

	public void persist(E e) {
		em.persist(e);
	}

	public void update(E e) {
		em.merge(e);
	}

	public void delete(E e) {
		em.remove(e);
	}

}