package org.nttdata.jsf.jsfstarter02.repository.abstract_repository;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.function.Supplier;
import java.util.stream.Stream;

abstract class AbstractRepositoryReflect<E> {
	protected String NAME_ENTITY;
	protected String NAME_ID;
	private Class<E> entityClass;
	protected Supplier<Class<E>> entity = () -> entityClass;

	public AbstractRepositoryReflect(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	protected void loadNameEntity() {
		boolean isPresent = entityClass.isAnnotationPresent(Entity.class);
		if (isPresent) {
			Entity annotation = entityClass.getAnnotation(Entity.class);
			NAME_ENTITY = annotation.name().isBlank() ? entityClass.getSimpleName() : annotation.name();
		}
	}

	protected void loadNameID() {
		Stream.of(entityClass.getDeclaredFields()).forEach(f -> {
			boolean present = f.isAnnotationPresent(Id.class);
			if (present) {
				NAME_ID = f.getName();
			}
		});
	}

}
