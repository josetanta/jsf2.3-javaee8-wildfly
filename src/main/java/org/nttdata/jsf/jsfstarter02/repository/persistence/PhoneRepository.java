package org.nttdata.jsf.jsfstarter02.repository.persistence;

import org.nttdata.jsf.jsfstarter02.models.Phone;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.CrudRepository;

import javax.ejb.Local;
import java.util.Set;

@Local
public interface PhoneRepository extends CrudRepository<Phone, Integer> {

	Set<Phone> findAllPhonesByOwnerId(Integer ownerId);
}
