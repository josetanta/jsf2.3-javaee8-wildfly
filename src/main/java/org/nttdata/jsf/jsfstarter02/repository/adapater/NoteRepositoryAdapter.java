package org.nttdata.jsf.jsfstarter02.repository.adapater;

import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.AbstractRepository;
import org.nttdata.jsf.jsfstarter02.repository.persistence.NoteRepository;

import javax.ejb.Stateful;
import javax.persistence.TemporalType;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Stateful
public class NoteRepositoryAdapter extends AbstractRepository<Note, Integer> implements NoteRepository {

	public NoteRepositoryAdapter() {
		super(Note.class);
	}

	@Override
	public List<Note> listNoteByDateAndCategory(Integer ownerId, Integer categoryId, Date date) {

		String jpql = "from Note n where n.owner.id=?1 and n.category.id=?2 and n.createdAt between ?3 and ?4";

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);

		List<Note> result = em.createQuery(jpql, Note.class)
			.setParameter(1, ownerId)
			.setParameter(2, categoryId)
			.setParameter(3, date, TemporalType.DATE)
			.setParameter(4, cal, TemporalType.DATE)
			.getResultList();

		return result;
	}

}
