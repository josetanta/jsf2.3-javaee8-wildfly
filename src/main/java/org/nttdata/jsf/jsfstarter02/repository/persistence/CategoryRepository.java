package org.nttdata.jsf.jsfstarter02.repository.persistence;

import org.nttdata.jsf.jsfstarter02.models.Category;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.CrudRepository;

import javax.ejb.Local;

@Local
public interface CategoryRepository extends CrudRepository<Category, Integer> {

}
