package org.nttdata.jsf.jsfstarter02.repository.adapater;

import org.nttdata.jsf.jsfstarter02.models.Menu;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.AbstractRepository;
import org.nttdata.jsf.jsfstarter02.repository.persistence.MenuRepository;

import javax.ejb.Stateful;

@Stateful
public class MenuRepositoryAdapter extends AbstractRepository<Menu, Integer> implements MenuRepository {

	public MenuRepositoryAdapter() {
		super(Menu.class);
	}

}
