package org.nttdata.jsf.jsfstarter02.repository.abstract_repository;

import java.util.Collection;
import java.util.Optional;

public interface CrudRepository<E, ID> {
	Optional<E> findById(ID id);

	Collection<E> findAll();

	void persist(E e);

	void update(E e);

	void delete(E e);
}
