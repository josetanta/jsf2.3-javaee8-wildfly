package org.nttdata.jsf.jsfstarter02.repository.persistence;

import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.repository.abstract_repository.CrudRepository;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;

@Local
public interface NoteRepository extends CrudRepository<Note, Integer> {
	List<Note> listNoteByDateAndCategory(Integer ownerId, Integer categoryId, Date date);
}
