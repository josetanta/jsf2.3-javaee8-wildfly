package org.nttdata.jsf.jsfstarter02.services.authenticate;

import org.nttdata.jsf.jsfstarter02.exceptions.AuthenticationFailedException;

import javax.ejb.Local;

@Local
public interface UserDetailsService {
	UserDetails authenticate(UserDetails details) throws AuthenticationFailedException;
}
