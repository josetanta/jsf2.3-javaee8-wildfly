package org.nttdata.jsf.jsfstarter02.services;

import lombok.extern.slf4j.Slf4j;
import org.nttdata.jsf.jsfstarter02.exceptions.AuthenticationFailedException;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.services.authenticate.UserDetails;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Slf4j
@Stateless
public class UserServiceAdapater implements UserService {

	@PersistenceContext
	private EntityManager em;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void register(User user) {
		em.persist(user);
		log.info("info user [{}]", user);
	}

	@Override
	public UserDetails authenticate(UserDetails details) throws AuthenticationFailedException {
		String hql = "select u from User u where u.username=?1 and u.password=?2";

		UserDetails u = em.createQuery(hql, UserDetails.class)
			.setParameter(1, details.getUsername())
			.setParameter(2, details.getPassword())
			.getSingleResult();

		return Optional.ofNullable(u).orElseThrow();
	}
}
