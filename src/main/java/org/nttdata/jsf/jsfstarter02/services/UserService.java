package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.services.authenticate.UserDetailsService;

import javax.ejb.Local;

@Local
public interface UserService extends UserDetailsService {
	void register(User user);
}
