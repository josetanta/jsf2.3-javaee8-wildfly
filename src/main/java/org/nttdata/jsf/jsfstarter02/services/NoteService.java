package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.commons.CrudService;
import org.nttdata.jsf.jsfstarter02.models.Note;

import javax.ejb.Local;
import java.util.Date;
import java.util.List;

@Local
public interface NoteService extends CrudService<Note, Integer> {
	List<Note> searchNotesByCategoryAndDate(Integer categoryId, Date date);
}
