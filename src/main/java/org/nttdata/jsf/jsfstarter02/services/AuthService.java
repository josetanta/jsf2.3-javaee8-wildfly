package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.services.authenticate.UserDetails;

public interface AuthService {
	void authenticate(UserDetails details);
}
