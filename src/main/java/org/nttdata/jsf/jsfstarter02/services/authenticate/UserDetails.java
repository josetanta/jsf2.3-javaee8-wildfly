package org.nttdata.jsf.jsfstarter02.services.authenticate;

public interface UserDetails {
	String getUsername();

	String getPassword();
}
