package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.models.Note;
import org.nttdata.jsf.jsfstarter02.models.User;
import org.nttdata.jsf.jsfstarter02.repository.persistence.NoteRepository;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Stateless
public class NoteServiceAdapter implements NoteService {

	@Inject
	private NoteRepository noteRepository;

	@Override
	public List<Note> list() {
		return (List<Note>) noteRepository.findAll();
	}

	@Override
	public void create(Note obj) {
		noteRepository.persist(obj);
	}

	@Override
	public void update(Note obj, Integer id) {
		noteRepository.update(obj);
	}

	@Override
	public void delete(Integer id) {
		Note note = findById(id);
		noteRepository.delete(note);
	}

	@Override
	public Note findById(Integer id) {
		return noteRepository.findById(id).orElseThrow();
	}

	@Override
	public List<Note> searchNotesByCategoryAndDate(Integer categoryId, Date date) {
		User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("session-us");

		Optional.of(user).orElseThrow();

		return noteRepository.listNoteByDateAndCategory(user.getOwner().getId(), categoryId, date);
	}

}
