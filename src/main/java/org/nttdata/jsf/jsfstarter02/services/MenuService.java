package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.commons.CrudService;
import org.nttdata.jsf.jsfstarter02.models.Menu;

import javax.ejb.Local;

@Local
public interface MenuService extends CrudService<Menu, Integer> {

}
