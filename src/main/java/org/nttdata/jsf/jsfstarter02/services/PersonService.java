package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.models.Person;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PersonService {

	@PersistenceContext
	private EntityManager em;

	public void registerPerson(Person person) {
		em.persist(person);
	}

	public List<Person> listOfPerson() {
		return em.createQuery("from Person", Person.class)
			.getResultList();
	}
}
