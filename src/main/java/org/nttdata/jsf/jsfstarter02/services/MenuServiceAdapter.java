package org.nttdata.jsf.jsfstarter02.services;

import org.nttdata.jsf.jsfstarter02.models.Menu;
import org.nttdata.jsf.jsfstarter02.repository.persistence.MenuRepository;

import javax.ejb.Stateful;
import javax.inject.Inject;
import java.util.List;

@Stateful
public class MenuServiceAdapter implements MenuService {

	@Inject
	private MenuRepository menuRepository;

	@Override
	public List<Menu> list() {
		return (List<Menu>) menuRepository.findAll();
	}

	@Override
	public void create(Menu obj) {
		menuRepository.persist(obj);
	}

	@Override
	public void update(Menu obj, Integer id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Integer id) {
		Menu menu = findById(id);
		menuRepository.delete(menu);
	}

	@Override
	public Menu findById(Integer id) {
		return menuRepository.findById(id).orElseThrow();
	}
}
